/**
 * ACK packets are sent to acknowledge WRQ and DATA packets.
 *
 * Structure:
 * 2 bytes   2 bytes
 * ------------------
 *| Opcode | Block # |
 * ------------------
 */

import java.net.*;

public class AckPacket extends TFTPPacket {
	private int blockNum;

	/**
	 * TFTPPacket constructor for ACK packets based on the components.
	 *
	 * @param blockNum block number of the data
	 * @param address  destination address of the packet
	 * @param port     destiantion prot of the packet
	 */
	public AckPacket(int blockNum, InetAddress address, int port) {
		this.blockNum = blockNum;
		updatePacketData();
		this.udpPacket = new DatagramPacket(this.data, this.length, address,
		                                    port);
	}

	/**
	 * TFTPPacket constructor for ACK packets based on a datagram packet
	 *
	 * @param  udpPacket                 packet that contains a TFTP-compliant
	 *                                   byte array
	 * @throws IllegalArgumentException  if format of packet data is invalid
	 */
	public AckPacket(DatagramPacket udpPacket) throws IllegalArgumentException{
		if (udpPacket.getLength() != 4) {
			throw new IllegalArgumentException("Invalid length expected " +
			                                   "between " + 4 + " and " + 4);
		}

		this.udpPacket = udpPacket;
		this.data = udpPacket.getData();
		this.length = udpPacket.getLength();

		this.blockNum = ((data[2] & 0xFF) << 8) | (data[3] & 0xFF);
	}

	/**
	 * Updates the data and length fields.
	 */
	public void updatePacketData() {
		int dataInd = 0;
		this.data = new byte[4];
		this.data[dataInd++] = 0;
		this.data[dataInd++] = 4;
		this.data[dataInd++] = (byte) (blockNum >> 8);
		this.data[dataInd] = (byte) blockNum;

		this.length = this.data.length;
	}

	public String getDetails() {
		String details = "";
		details += this + "\n";
		details += "Opcode: " + this.getOpcode() + "\n";
		details += "BlockNum: " + blockNum;

		return details;
	}

	public int getBlockNum() {
		return blockNum;
	}

	public void setBlockNum(int blockNum) {
		this.blockNum = blockNum;
		updatePacketData();
		udpPacket.setData(data);
	}

	public String toString() {
		return "ACK";
	}
}
