/**
 * Handles sending and receiving packets for a specific socket. Determines
 * whether a received packet contains an error.
 */

import java.io.*;
import java.net.*;
import java.util.Arrays;

public class PacketHandler {
	DatagramSocket sendReceiveSocket;
	DatagramPacket receivePacket;
	TFTPPacket sendPacket;
	boolean verbose;

	/**
	 * Constructor for the PacketHandler.
	 *
	 * @param verbose boolean representing whether verbose mode is on or off
	 */
	public PacketHandler(boolean verbose) {
		/* Create the socket for sending and receiving packets to and from a
		 * server. */
		try {
			sendReceiveSocket = new DatagramSocket();
		} catch (SocketException se) {
			se.printStackTrace();
			System.exit(1);
		}

		this.verbose = verbose;
	}

	/**
	 * Constructor for the PacketHandler.
	 *
	 * @param port    port number for the socket to bind to
	 * @param verbose boolean representing whether verbose mode is on or off
	 */
	public PacketHandler(int port, boolean verbose) {
		/* Create the socket for sending and receiving packets to and from a
		 * server. */
		try {
			sendReceiveSocket = new DatagramSocket(port);
		} catch (SocketException se) {
			se.printStackTrace();
			System.exit(1);
		}

		this.verbose = verbose;
	}

	/**
	 * Create the packet from packet data.
	 *
	 * @param udpPacket UDP packet containg data from which to create TFTP packet
	 */
	public TFTPPacket createPacket(DatagramPacket udpPacket) {
		TFTPPacket p;
		int opcode = udpPacket.getData()[1];

		try {
			switch (opcode) {
				case 1: case 2:
					return new RQPacket(udpPacket);
				case 3:
					return new DataPacket(udpPacket);
				case 4:
					return new AckPacket(udpPacket);
				case 5:
					return new ErrorPacket(udpPacket);
				default:
					System.out.println("Packet contains an invalid opcode.");
					return null;
			}
		} catch (IllegalArgumentException e) {
			String msg = e.getMessage();
			String error = msg.split("\\s")[1];
			InetAddress address = udpPacket.getAddress();
			int port = udpPacket.getPort();

			if (error.equals("format")) {
				invalidFormatError(Configuration.packetType[opcode],
				                   Configuration.packetType[opcode], address,
				                   port);
			}
			else if (error.equals("length")) {
				invalidLengthError(Integer.parseInt(msg.split("\\s")[4]),
				                   Integer.parseInt(msg.split("\\s")[6]),
				                   udpPacket.getLength(), address, port);
			}
			else if (error.equals("mode")) {
				invalidModeError(msg.split("\\s")[2], address, port);
			}
			else if (error.equals("filename")) {
				invalidFilenameError(address, port);
			}
			else if (error.equals("fileexists")) {
				fileExistsError(address, port);
			}
			else if (error.equals("filenotfound")) {
				fileNotFoundError(address, port);
			}
			else if (error.equals("diskfull")) {
				diskFullError(address, port);
			}
			else if (error.equals("noaccess")) {
				noAccessError(address, port);
			}

			return null;
		}
	}

	/**
	 * Send an ERROR packet to indicate a received packet with an invalid
	 * mode.
	 *
	 * @param mode     mode of the received packet
	 * @param address  destination address of the ERROR packet
	 * @param port     destination port of the ERROR packet
	 */
	public void invalidModeError(String mode, InetAddress address, int port) {
		String msg = "Expected: RQ packet with valid mode. Received: RQ " +
		             "packet with mode " + mode + ".";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(4, msg, address, port));
	}

	/**
	 * Send an ERROR packet to indicate a received packet with an invalid
	 * filename.
	 *
	 * @param address   destination address of the ERROR packet
	 * @param port      destination port of the ERROR packet
	 */
	public void invalidFilenameError(InetAddress address, int port) {
		String msg = "Expected: RQ packet with valid filename. Received: RQ " +
		             "packet with invalid filename.";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(4, msg, address, port));
	}

	/**
	 * Send an ERROR packet to indicate a received packet with an invalid
	 * length.
	 *
	 * @param minLength       minimum desirable length
	 * @param maxLength       maximum desirable length
	 * @param receivedLength  length of the received packet
	 * @param address         destination address of the ERROR packet
	 * @param port            destination port of the ERROR packet
	 */
	public void invalidLengthError(int minLength, int maxLength,
	                               int receivedLength, InetAddress address,
	                               int port) {
		String msg = "Expected: packet with min length " + minLength + " and " +
		             "max length " + maxLength + ". " +
		             "Received: packet with length " + receivedLength;
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(4, msg, address, port));
	}

	/**
	 * Send an ERROR packet to indicate a received packet with an incorrect
	 * type.
	 *
	 * @param type          desired packet type
	 * @param receivedType  received packet type
	 * @param address       destination address of the ERROR packet
	 * @param port          destination port of the ERROR packet
	 */
	public void invalidFormatError(String type, String receivedType,
	                               InetAddress address, int port) {
		String msg = "Expected: " + type + " packet with correct format. " +
		             "Received: " + receivedType + " packet with incorrect " +
		             "format.";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(4, msg, address, port));
	}

	/**
	 * Send an ERROR packet to indicate a received packet with an incorrect
	 * type.
	 *
	 * @param opcode          desired opcode
	 * @param receivedOpcode  opcode of the received packet
	 * @param address         destination address of the ERROR packet
	 * @param port            destination port of the ERROR packet
	 */
	public void invalidTypeError(String type, String receivedType,
	                             InetAddress address, int port) {
		String msg = "Expected: " + type + " packet. Received: " + receivedType
		             + " packet.";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(4, msg, address, port));
	}

	/**
	 * Send an ERROR packet to indicate a file with that name already exists
	 *
	 * @param address   destination address of the ERROR packet
	 * @param port      destination port of the ERROR packet
	 */
	public void fileExistsError(InetAddress address, int port) {
		String msg = "A file with that name already exists";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(6, msg, address, port));
	}

	/**
	 * Send an ERROR packet to indicate that no file with that name exists
	 *
	 * @param address   destination address of the ERROR packet
	 * @param port      destination port of the ERROR packet
	 */
	public void fileNotFoundError(InetAddress address, int port) {
		String msg = "No file with that name was found";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(1, msg, address, port));
	}

	/**
	 * Send an ERROR packet to indicate that the receiver's memory is full
	 *
	 * @param address   destination address of the ERROR packet
	 * @param port      destination port of the ERROR packet
	 */
	public void diskFullError(InetAddress address, int port) {
		String msg = "The disk of the receiver is full";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(3, msg, address, port));
	}

	/**
	 * Send an ERROR packet to indicate that the user cannot access that file
	 *
	 * @param address   destination address of the ERROR packet
	 * @param port      destination port of the ERROR packet
	 */
	public void noAccessError(InetAddress address, int port) {
		String msg = "No permission to access that file";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(2, msg, address, port));
	}

	/**
	 * Check packet for incorrect block number.
	 *
	 * @param packet    DATA packet to be checked
	 * @param blockNum  block that the data should be from
	 */
	public void incorrectBlockNum(int opcode, int blockNum, int receivedBlockNum,
	                              InetAddress address, int port) {
		String type = opcode == Configuration.DATA ? "DATA" : "ACK";
		String msg = "Expected: " + type + " packet with block " + blockNum +
		             ". Received: " + type + " packet with block "
		             + receivedBlockNum + ".";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(4, msg, address, port));
	}

	/**
	 * Send an ERROR packet to indicate an unknown transfer id error.
	 *
	 * @param address          desired address
	 * @param port             desired port
	 * @param receivedAddress  address of received packet
	 * @param receivedPort     port of received packet
	 */
	public void unknownTidError(InetAddress address, int port,
	                            InetAddress receivedAddress, int receivedPort) {
		String msg = "Expected: packet from " + address + ":" + port +
		             ". Received: packet from " + receivedAddress + ":" +
		             receivedPort + ".";
		if (verbose) {
			System.out.println("\nReceived packet containing error. " + msg);
		}
		send(new ErrorPacket(5, msg, receivedAddress, receivedPort));
	}

	/**
	 * Sends a TFTP packet to the specified address and port.
	 *
	 * @param tftpPacket TFTP packet being sent
	 * @return           return code to indicate whether the send was successful
	 */
	public int send(TFTPPacket tftpPacket) {
		/* Outputs packet info if in verbose mode. */
		if (verbose) {
			System.out.println("Sending " + tftpPacket + " packet...");
			Util.printSendPacketInfo(tftpPacket.getUdpPacket());
			System.out.println("TFTP DETAILS:\n" + tftpPacket.getDetails());
		}

		/* Attempts to send request packet to the server. */
		try {
			sendReceiveSocket.send(tftpPacket.getUdpPacket());
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		if (verbose) {
			System.out.println(tftpPacket + " packet sent.");
			System.out.println();
		}

		sendPacket = tftpPacket;

		return 0;
	}

	/**
	 * Receives a packet and converts it to a TFTP packet.
	 *
	 * @param  opcode   desired opcode of the packet being received
	 * @param  timeout  socket timeout time
	 * @return          TFTP packet representing the received packet or null if
	 *                  there was an error in the received packet
	 */
	public TFTPPacket receive(int opcode, int timeout) {
		return receive(opcode, timeout, true);
	}

	/**
	 * Receives a packet and converts it to a TFTP packet.
	 *
	 * @param  opcode    desired opcode of the packet being received
	 * @param  timeout   socket timeout time
	 * @param  printWait set to false to skip printing waiting message
	 * @return           TFTP packet representing the received packet or null if
	 *                   there was an error in the received packet
	 */
	public TFTPPacket receive(int opcode, int timeout, boolean printWait) {
		String type;
		int minLength;
		byte data[] = new byte[Configuration.MAX_PACKET_SIZE + 1];


		/* Create DatagramPacket to receive packet. */
		receivePacket = new DatagramPacket(data, data.length);

		switch (opcode) {
			case 0: case 1: case 2: case 3: case 4:
				minLength = 4;
				break;
			case 5:
				minLength = 5;
				break;
			default:
				System.out.println("Invalid opcode passed in.");
				return null;
		}

		type = Configuration.packetType[opcode];

		while (true) {
			/* Outputs system info if in verbose mode. */
			if (verbose && printWait) {
				System.out.println("Waiting for " + Configuration.packetType[opcode]
				                   + " packet...");
			}

			/* Wait for the corresponding acknowledgement packet before
			 * proceeding */
			try {
				/* Set socket timeout to specified timeout. */
				sendReceiveSocket.setSoTimeout(timeout);
				/* Wait to receive packet. */
				sendReceiveSocket.receive(receivePacket);
				/* Set socket timeout to be infinite again. */
				sendReceiveSocket.setSoTimeout(0);
				break;
			} catch (SocketTimeoutException ste) {
				/* If receive times out waiting for an ACK for a DATA, resend. */
				if (sendPacket != null && (sendPacket.toString().equals("DATA") ||
				    sendPacket.toString().equals("RRQ") ||
				    sendPacket.toString().equals("WRQ"))) {
					if (verbose) {
						System.out.println("\nReceive timed out while waiting for" +
						                   " packet.");
					}
					send(sendPacket);
				} else if (opcode == Configuration.RQ) {
					return null;
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		/* Outputs packet info if in verbose mode. */
		if (verbose) {
			System.out.println("Packet received.");
			Util.printReceivePacketInfo(receivePacket);
		}

		byte[] receiveData = Arrays.copyOf(receivePacket.getData(),
		                                   receivePacket.getLength());

		/* Check if opcode of received packet matches desired opcode. */
		if ((opcode == 0 && receiveData[1] != 1 && receiveData[1] != 2) ||
		    (opcode != 0 && receiveData[1] != 5 && receiveData[1] != opcode)) {
			invalidTypeError(type, Configuration.packetType[receiveData[1]],
			                 receivePacket.getAddress(),
			                 receivePacket.getPort());
			return null;
		}

		TFTPPacket tftpPacket = createPacket(receivePacket);
		if (tftpPacket != null && verbose) {
			System.out.println("TFTP DETAILS:\n" + tftpPacket.getDetails());
			System.out.println();
		}

		return tftpPacket;
	}

	/**
	 * Receives a packet and converts it to a TFTP packet.
	 *
	 * @param  opcode desired opcode of the packet being received
	 * @return        TFTP packet representing the received packet or null if
	 *                there was an error in the received packet
	 */
	public TFTPPacket receive(int opcode) {
		return receive(opcode, 0);
	}

	/**
	 * Parses received ERROR packet for desired packet info.
	 *
	 * @param errorPacket  Received ERROR packet.
	 * @return             Desired TFTP packet if DATA error.
	 */
	public TFTPPacket receivedError(ErrorPacket errorPacket) {
		String errMsg = errorPacket.getErrMsg();
		/* errMsg format: Expected: TYPE packet with FIELD VALUE. Received: TYPE
		 * packet with FIELD VALUE. */
		String[] temp = errMsg.split("\\W\\s");
		if (temp.length < 2) {
			return errorPacket;
		}
		String[] expected = temp[1].split("\\s");
		String type = expected[0];

		if (type.equals("DATA")) {
			if (expected.length == 5) {
				int blockNum;
				try {
					blockNum = Integer.parseInt(expected[4]);
				} catch (NumberFormatException e) {
					return null;
				}
				InetAddress address = errorPacket.getUdpPacket().getAddress();
				int port = errorPacket.getUdpPacket().getPort();
				DataPacket dataPacket = new DataPacket(blockNum, new byte[0],
				                                       address, port);
				return dataPacket;
			}
		}

		return null;
	}

	public DatagramSocket getSocket() {
		return sendReceiveSocket;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public void resetSendPacket() {
		sendPacket = null;
	}
}
