/**
 * Error simulator for the TFTP system. Simulates packet errors.
 */

import java.io.*;
import java.net.*;
import java.util.*;


public class ErrorSimulator {
	private static final int DELAY = 2000; //2 second delay

	private static enum Error {
		NONE,

		OPCODE_RQ, OPCODE_DATA, OPCODE_ACK, MODE, BLOCKNUM_DATA, BLOCKNUM_ACK,
		FORMAT_RQ, FORMAT_DATA, FORMAT_ACK, SHORT_DATA, SHORT_ACK, LONG_DATA,
		LONG_ACK, TID_DATA, TID_ACK,

		DUPLICATE_ACK, DELAY_ACK, DROP_ACK, DUPLICATE_DATA, DELAY_DATA,
		DROP_DATA, DUPLICATE_RQ, DELAY_RQ, DROP_RQ
	}

	private static Error[] needBlockNum = {
		Error.OPCODE_DATA, Error.OPCODE_ACK, Error.BLOCKNUM_DATA,
		Error.BLOCKNUM_ACK, Error.FORMAT_DATA, Error.FORMAT_ACK,
		Error.SHORT_DATA, Error.SHORT_ACK, Error.LONG_DATA, Error.LONG_ACK,
		Error.TID_DATA, Error.TID_ACK, Error.DUPLICATE_ACK, Error.DELAY_ACK,
		Error.DROP_ACK, Error.DUPLICATE_DATA, Error.DELAY_DATA, Error.DROP_DATA
	};

	private DatagramSocket sendReceiveSocket;

	private DatagramPacket sendPacket, receivePacket;

	private InetAddress clientAddress, serverAddress;

	private int errSimPort, clientPort, serverPort, connectionPort, blockNum,
	delay;

	private Error error;

	private boolean verbose, updateConnectionPort;

	/**
	 * Constructor for the error simulator.
	 *
	 * @param serverAddress IP address of the server
	 * @param errSimPort    port that the error simulator will listen on
	 * @param serverPort    port of the server
	 * @param error         type of error to simulate
	 * @param blockNum      block number to simulate error on, if necessary
	 * @param verbose       boolean specifying whether to print packet
	 *                      information during transfer
	 */
	public ErrorSimulator(InetAddress serverAddress, int errSimPort,
	                      int serverPort, Error error, int blockNum,
	                      boolean verbose, int delay) {
		this.serverAddress = serverAddress;
		this.errSimPort = errSimPort;
		this.serverPort = serverPort;
		this.verbose = verbose;
		this.error = error;
		this.blockNum = blockNum;
		this.delay = delay;
		updateConnectionPort = false;

		try {
			/* Create the socket that sends and receives packets. */
			sendReceiveSocket = new DatagramSocket(errSimPort);
		} catch (SocketException se) {
			se.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Prompt user to choose a Netword Error.
	 *
	 * @param  s scanner for user input
	 * @return   error chosen by user
	 */
	public static Error getNetworkError(Scanner s){

		System.out.println("\nChoose a Network Error:");
		System.out.println("1. Duplicate packet (RRQ/WRQ)");
		System.out.println("2. Duplicate packet (DATA)");
		System.out.println("3. Duplicate packet (ACK)");
		System.out.println("4. Delay packet (RRQ/WRQ)");
		System.out.println("5. Delay packet (DATA)");
		System.out.println("6. Delay packet (ACK)");
		System.out.println("7. Drop packet (RRQ/WRQ)");
		System.out.println("8. Drop packet (DATA)");
		System.out.println("9. Drop packet (ACK)");

		while (true) {
			try {
				int error = Integer.parseInt(s.next());
				switch (error) {
					case 1:
						return Error.DUPLICATE_RQ;
					case 2:
						return Error.DUPLICATE_DATA;
					case 3:
						return Error.DUPLICATE_ACK;
					case 4:
						return Error.DELAY_RQ;
					case 5:
						return Error.DELAY_DATA;
					case 6:
						return Error.DELAY_ACK;
					case 7:
						return Error.DROP_RQ;
					case 8:
						return Error.DROP_DATA;
					case 9:
						return Error.DROP_ACK;
					default:
						System.out.println("\nInvalid input. Input 1..9 to " +
						                   "choose an error.");
						break;
				}
			} catch (NumberFormatException | NullPointerException e) {
				System.out.println("\nInvalid input. Input 1..9 to choose " +
				                   "an error.");
			}
		}
	}

	/**
	 * Prompt user to choose an error.
	 *
	 * @param  s scanner for user input
	 * @return   error chosen by user
	 */
	public static Error getTFTPError(Scanner s) {
		System.out.println("\nChoose a TFTP error:");
		System.out.println("1. Invalid opcode (RRQ/WRQ)");
		System.out.println("2. Invalid opcode (DATA)");
		System.out.println("3. Invalid opcode (ACK)");
		System.out.println("4. Invalid RRQ/WRQ mode");
		System.out.println("5. Invalid block number (DATA)");
		System.out.println("6. Invalid block number (ACK)");
		System.out.println("7. Invalid format (RRQ/WRQ)");
		System.out.println("8. Invalid format (DATA)");
		System.out.println("9. Invalid format (ACK)");
		System.out.println("10. Packet too short (DATA)");
		System.out.println("11. Packet too short (ACK)");
		System.out.println("12. Packet too long (DATA)");
		System.out.println("13. Packet too long (ACK)");
		System.out.println("14. Unknown transfer ID (DATA)");
		System.out.println("15. Unknown transfer ID (ACK)");

		while (true) {
			try {
				int error = Integer.parseInt(s.next());

				switch (error) {
					case 1:
						return Error.OPCODE_RQ;
					case 2:
						return Error.OPCODE_DATA;
					case 3:
						return Error.OPCODE_ACK;
					case 4:
						return Error.MODE;
					case 5:
						return Error.BLOCKNUM_DATA;
					case 6:
						return Error.BLOCKNUM_ACK;
					case 7:
						return Error.FORMAT_RQ;
					case 8:
						return Error.FORMAT_DATA;
					case 9:
						return Error.FORMAT_ACK;
					case 10:
						return Error.SHORT_DATA;
					case 11:
						return Error.SHORT_ACK;
					case 12:
						return Error.LONG_DATA;
					case 13:
						return Error.LONG_ACK;
					case 14:
						return Error.TID_DATA;
					case 15:
						return Error.TID_ACK;
					default:
						System.out.println("\nInvalid input. Input 1..15 to " +
						                   "choose an error.");
						break;
				}
			} catch (NumberFormatException | NullPointerException e) {
				System.out.println("\nInvalid input. Input 1..15 to choose " +
				                   "an error.");
			}
		}
	}

	/**
	 * Prompt user to input block number of the packet to be corrupted.
	 *
	 * @param  s scanner for user input
	 * @return   blocknum specified by user
	 */
	public static int getBlockNum(Scanner s) {
		int blockNum;

		System.out.println("\nEnter the block number of the packet " +
		                   "to be changed (Block 1 to 65535):");

		while(true) {
			try {
				blockNum = Integer.parseInt(s.next());
			} catch (NumberFormatException | NullPointerException e) {
				System.out.println("\nInvalid input. Valid blocknum: 1..65535.");
				continue;
			}

			if (blockNum < 1 || blockNum > 65535) {
				System.out.println("\nInvalid input. Valid blocknum: 1..65535.");
			}
			else {
				return blockNum;
			}
		}
	}

	/**
	 * Changes the opcode of the given packet.
	 *
	 * @param  receivePacket  packet received
	 * @param  opcode         opcode that the packet will be changed to
	 */
	public void changeOpcode(DatagramPacket receivePacket, int opcode) {
		byte[] data = receivePacket.getData();
		data[1] = (byte) opcode;
		receivePacket.setData(data);
		if (verbose) {
			System.out.println("\nChanged opcode to " + opcode);
		}
	}

	/**
	 * Changes the mode of the given packet.
	 *
	 * @param  receivePacket  packet received
	 * @param  mode           mode that the packet will be changed to
	 */
	public void changeMode(DatagramPacket receivePacket, String mode) {
		switch ((int) receivePacket.getData()[1]) {
			case Configuration.RRQ: case Configuration.WRQ:
				RQPacket rqPacket = new RQPacket(receivePacket);
				rqPacket.setMode(mode);
				receivePacket.setData(rqPacket.getData());
				break;
			default:
				return;
		}

		if (verbose) {
			System.out.println("\nChanged mode to " + mode);
		}
	}

	/**
	 * Changes the blocknum of the given packet.
	 *
	 * @param  receivePacket  packet received
	 * @param  blockNum       blocknum that the packet will be changed to
	 */
	public void changeBlockNum(DatagramPacket receivePacket, int blockNum) {
		switch ((int) receivePacket.getData()[1]) {
			case Configuration.DATA:
				DataPacket dataPacket = new DataPacket(receivePacket);
				dataPacket.setBlockNum(blockNum);
				receivePacket.setData(dataPacket.getData());
				break;
			case Configuration.ACK:
				AckPacket ackPacket = new AckPacket(receivePacket);
				ackPacket.setBlockNum(blockNum);
				receivePacket.setData(ackPacket.getData());
				break;
			default:
				return;
		}

		if (verbose) {
			System.out.println("\nChanged blocknum to " + blockNum);
		}
	}

	/**
	 * Changes the format of the given packet by adding a null byte in the byte
	 * array.
	 *
	 * @param  receivePacket  packet received
	 */
	public void changeFormat(DatagramPacket receivePacket) {
		byte[] data = new byte[receivePacket.getLength() + 1];
		int dataInd = 0;
		for (byte b : Arrays.copyOf(receivePacket.getData(),
		                            receivePacket.getLength())) {
			if (dataInd == 3) {
				data[dataInd++] = 0;
			} else {
				data[dataInd++] = b;
			}
		}
		receivePacket.setData(data);

		if (verbose) {
			System.out.println("\nAdded extra null terminator to packet data.");
		}
	}

	/**
	 * Shorten the length of the data byte array in a given packet
	 *
	 * @param  receivePacket  packet received
	 * @param  length         new length of byte array
	 */
	public void shortenPacket(DatagramPacket receivePacket, int length) {
		if (length > receivePacket.getLength()) {
			length = receivePacket.getLength();
		}
		byte[] data = receivePacket.getData();
		receivePacket.setData(Arrays.copyOf(data, length));
	}

	/**
	 * Extend the length of the data byte array in a given packet
	 *
	 * @param  receivePacket  packet received
	 * @param  length         new length of byte array
	 */
	public void lengthenPacket(DatagramPacket receivePacket, int length) {
		int oldLength = receivePacket.getLength();
		if (length < oldLength) {
			length = oldLength;
		}
		byte[] oldData = receivePacket.getData();
		byte[] newData = new byte[length];
		for (int i = 0; i < oldLength; i++) {
			newData[i] = oldData[i];
		}
		for (int i = oldLength; i < length; i++) {
			newData[i] = (byte) '~';
		}
		receivePacket.setData(newData);
	}

	/**
	 * Transfers packet from a source to a destination.
	 */
	public void transferPacket() {
		InetAddress destAddress;
		int destPort;
		boolean changePacket = false;
		boolean netError = false;

		/* Create the packet that will receive data from the source. */
		byte data[] = new byte[Configuration.MAX_PACKET_SIZE];
		receivePacket = new DatagramPacket(data, data.length);

		if (verbose) {
			System.out.println("\nERROR_SIM: Waiting for packet...");
		}

		/* Wait for a packet on the specified port. */
		try {
			sendReceiveSocket.receive(receivePacket);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		if (verbose) {
			System.out.println("\nERROR_SIM: Packet received.");
			Util.printReceivePacketInfo(receivePacket);
			System.out.println();
		}

		if (updateConnectionPort) { /* New client-server connection after RQ
		                               sent. */
			connectionPort = receivePacket.getPort();
			updateConnectionPort = false;
		}

		/* RQ packet received. */
		if (data[1] <= Configuration.WRQ) {
			if (clientAddress == null) { /* First request from client. */
				clientAddress = receivePacket.getAddress();
				clientPort = receivePacket.getPort();
			}

			destAddress = serverAddress;
			destPort = serverPort;
			updateConnectionPort = true;

			switch (error) {
				case OPCODE_RQ:
					changeOpcode(receivePacket, Configuration.DATA);
					changePacket = true;
					break;
				case MODE:
					changeMode(receivePacket, "random");
					changePacket = true;
					break;
				case FORMAT_RQ:
					changeFormat(receivePacket);
					changePacket = true;
					break;
				case DELAY_RQ: case DUPLICATE_RQ: case DROP_RQ:
					netError = true;
					break;
				default:
					break;
			}
		}
		else {
			/* Get the packet's address and port. These will be used to
			 * determine where to pass the packet to.*/
			InetAddress sourceAddress = receivePacket.getAddress();
			int sourcePort = receivePacket.getPort();

			if (sourceAddress.equals(clientAddress) &&
			    sourcePort == clientPort) {
				/* Packet was received from client. */
				destAddress = serverAddress;
				destPort = connectionPort;
			}
			else { /* Packet was received from server. */
				destAddress = clientAddress;
				destPort = clientPort;
			}

			switch (data[1]) {
				case Configuration.DATA:
					if ((new DataPacket(receivePacket)).getBlockNum() == blockNum) {
						switch (error) {
							case OPCODE_DATA: case BLOCKNUM_DATA:
							case FORMAT_DATA: case TID_DATA:
							case SHORT_DATA: case LONG_DATA:
								changePacket = true;
								break;
							case DUPLICATE_DATA: case DELAY_DATA:
							case DROP_DATA:
								netError = true;
								break;
						}
					}
					break;
				case Configuration.ACK:
					if ((new AckPacket(receivePacket)).getBlockNum() == blockNum) {
						switch (error) {
							case OPCODE_ACK: case BLOCKNUM_ACK:
							case FORMAT_ACK: case TID_ACK:
							case SHORT_ACK: case LONG_ACK:
								changePacket = true;
								break;
							case DUPLICATE_ACK: case DELAY_ACK:
							case DROP_ACK:
								netError = true;
								break;
						}
					}
					break;
				default:
					break;
			}
		}

		if (changePacket) {
			switch (error) {
				case OPCODE_DATA: case OPCODE_ACK:
					changeOpcode(receivePacket, Configuration.RRQ);
					break;
				case BLOCKNUM_DATA: case BLOCKNUM_ACK:
					changeBlockNum(receivePacket, blockNum + 1);
					break;
				case FORMAT_DATA: case FORMAT_ACK:
					changeFormat(receivePacket);
					break;
				case SHORT_DATA: case SHORT_ACK:
					shortenPacket(receivePacket, 3);
					break;
				case LONG_DATA:
					lengthenPacket(receivePacket, 517);
					break;
				case LONG_ACK:
					lengthenPacket(receivePacket, 5);
					break;
				default:
					break;
			}
		}

		sendPacket = new DatagramPacket(receivePacket.getData(),
		                                receivePacket.getLength(), destAddress,
		                                destPort);

		if (verbose) {
			System.out.println("\nERROR_SIM: Sending packet...");
			Util.printSendPacketInfo(sendPacket);
		}

		/* Send the packet to the destination. */
		try {
			if (changePacket) {
				switch (error) {
					case TID_DATA: case TID_ACK:
						(new DatagramSocket()).send(sendPacket);
						break;
					default:
						sendReceiveSocket.send(sendPacket);
						break;
				}
				this.error = Error.NONE;
			} else if (netError) {
				switch (error) {
					case DROP_ACK: case DROP_DATA: case DROP_RQ:
						if (verbose) {
							System.out.println("\nERROR_SIM: Packet has been" +
							                   " dropped.");
						}
						this.error = Error.NONE;
						return;
					case DUPLICATE_DATA: case DUPLICATE_ACK: case DUPLICATE_RQ:
						if (verbose) {
							System.out.println("\nERROR_SIM: Duplicating " + 
							                   "packet.");
						}
						sendReceiveSocket.send(sendPacket);
						break;
					case DELAY_ACK: case DELAY_DATA: case DELAY_RQ:
						if (verbose) {
							System.out.println("\nERROR_SIM: Delaying packet " +
							                   "by " + delay +
							                   " milliseconds.");
						}

						/* Wait specified amount of time before sending packet.
						 */
						Thread.sleep(delay);
						break;
				}

				sendReceiveSocket.send(sendPacket);
				this.error = Error.NONE;
			} else {
				sendReceiveSocket.send(sendPacket);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		if (verbose) {
			System.out.println("\nERROR_SIM: Packet sent.");
		}
	}

	public static void main(String args[]) {
		InetAddress serverAddress = null;
		int serverPort = Configuration.SERVER_PORT;
		int errSimPort = Configuration.PROXY_PORT;
		Scanner s = new Scanner(System.in);
		String input;
		boolean verbose;

		while(true) {
			System.out.println("Choose a mode to run ErrorSimulator:");
			System.out.println("1. Verbose.");
			System.out.println("2. Quiet.");

			switch (s.next()) {
				case "1":
					verbose = true;
					System.out.println("Verbose mode selected.");
					break;
				case "2":
					verbose = false;
					System.out.println("Quiet mode selected.");
					break;
				default:
					System.out.println("Invalid input.");
					continue;
			}

			break;
		}

		Error error;
		System.out.println("\nChoose a Simulator function:");
		System.out.println("1. No Error.");
		System.out.println("2. Network Error");
		System.out.println("3. TFTP Error");
		while (true) {
			try {
				int selection = Integer.parseInt(s.next());
				switch (selection) {
					case 1:
						error = Error.NONE;
						break;
					case 2:
						error = getNetworkError(s);
						break;
					case 3:
						error = getTFTPError(s);
						break;
					default:
						System.out.println("\nInvalid input. Input 1..3 to " +
						                   "choose an error.");
						continue;
				}
			} catch (NumberFormatException | NullPointerException e) {
				System.out.println("\nInvalid input. Input 1..3 to choose " +
				                   "an error.");
				continue;
			}

			break;
		}

		int blockNum;
		if (Arrays.asList(needBlockNum).contains(error)) {
			blockNum = getBlockNum(s);
		} else {
			blockNum = -1;
		}

		int delay = 0;
		if (error == Error.DELAY_RQ || error == Error.DELAY_DATA ||
		    error == Error.DELAY_ACK) {
			while (true) {
				System.out.println("\nInput delay time in milliseconds:");
				try {
					delay = Integer.parseInt(s.next());
					break;
				} catch (NumberFormatException e) {
					System.out.println("\nInvalid input. Input a delay in " +
					                   "milliseconds.");
				}
			}
		}

		try {
			serverAddress = InetAddress.getByName(Configuration.HOSTNAME);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}

		/* Create the error simulator, then wait for packages indefinitely. */
		ErrorSimulator errSim = new ErrorSimulator(serverAddress, errSimPort,
		                                           serverPort, error, blockNum,
		                                           verbose, delay);

		// Loop until the send and receive method is finished
		while (true) {
			errSim.transferPacket();
		}
	}
}
