/**
 * This class contains values that are of use to multiple components of the TFTP
 * client-server system.
 */

public class Configuration {
	/* The address of the target host where the server is located */
	public static final String HOSTNAME = "localhost";

	/* The port where the server is located on the target host */
	public static final int SERVER_PORT = 69;

	/* The port where the proxy/errror simulator is located */
	public static final int PROXY_PORT = 23;

	/* The opcodes of the TFTP packets */
	public static final int RQ = 0, RRQ = 1, WRQ = 2, DATA = 3, ACK = 4,
	                        ERROR = 5;

	public static final int BLOCK_SIZE = 512;

	public static final int MAX_PACKET_SIZE = 516;

	public static final String[] packetType = new String[]{"RQ", "RRQ","WRQ",
	                                                       "DATA", "ACK",
	                                                       "ERROR"};
}
