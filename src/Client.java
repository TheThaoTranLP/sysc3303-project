/**
 * Client side for a file transfer system based on TFTP. The client prompts the
 * user to read or write a file to or from a server, respectively. The file is
 * transferred over a connection to the server. The user can continue
 * transferring files until they want to exit. The client, however, does not
 * support concurrent file transfers.
 */

import java.util.Scanner;
import java.util.Arrays;
import java.io.*;
import java.net.*;

public class Client {
	private PacketHandler packetHandler;

	private InetAddress destAddress;
	private int destPort;
	private int mode;
	private String directory;
	private boolean verbose;

	/**
	 * Constructor method for the Client class.
	 *
	 * @param destAddress   address of the destination
	 * @param destPort      port of the destination
	 * @param directory     directory of the files for the client
	 */
	public Client(InetAddress destAddress, int destPort, String directory) {
		this.destAddress = destAddress;
		this.destPort = destPort;
		this.directory = directory;
		mode = 1;

		packetHandler = new PacketHandler(verbose);
		updateVerbose();
	}

	/**
	 * Transfers a file to or from a server.
	 *
	 * @param filename  string representing the file to be transferred
	 * @param rqType    integer representing the request type
	 *                  (RRQ = 1; WRQ = 2)
	 */
	public void transferFile(String filename, int rqType) {
		/* Creates either a RRQ or WRQ packet to send to the server. */
		RQPacket rqPacket = new RQPacket(rqType, filename, "netascii",
		                                 destAddress, destPort);

		/* Declare FileTransfer object to transfer files with server. */
		FileTransfer fileTransfer;

		packetHandler.send(rqPacket);

		switch (rqType) {
			case 1: /* Read file from server. */
				fileTransfer = new FileTransfer(packetHandler, filename,
				                                directory, verbose);
				fileTransfer.receiveFile();
				break;
			case 2: /* Writes file to server. */
				TFTPPacket tftpPacket = packetHandler.receive(4, 5000);
				if (tftpPacket == null) {
					System.out.println("File transfer not completed.");
				} else if (tftpPacket.toString().equals("ERROR")) {
					ErrorPacket errorPacket = (ErrorPacket) tftpPacket;
					System.out.println(errorPacket.getErrMsg());
					System.out.println("File transfer not completed.");
				} else {
					DatagramPacket udpPacket = tftpPacket.getUdpPacket();
					fileTransfer = new FileTransfer(packetHandler, filename,
					                                directory,
					                                udpPacket.getAddress(),
					                                udpPacket.getPort(),
					                                verbose);
					fileTransfer.sendFile();
				}
				break;
			default:
				System.out.println("Incorrect opcode. Only read (1) or write" +
				                   " (2) are valid.");
				break;
		}

		packetHandler.resetSendPacket();
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
		try {
			if (mode == 1 || mode == 2) {
				destAddress = InetAddress.getByName(Configuration.HOSTNAME);
				destPort = Configuration.SERVER_PORT;
			} else if (mode == 3 || mode == 4) {
				destAddress = InetAddress.getLocalHost();
				destPort = Configuration.PROXY_PORT;
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}
		updateVerbose();
	}

	private void updateVerbose() {
		if (mode == 1 || mode == 3) {
			verbose = false;
			packetHandler.setVerbose(verbose);
		} else if (mode == 2 || mode == 4) {
			verbose = true;
			packetHandler.setVerbose(verbose);
		}
	}

	public static void main( String args[] ) {
		String dir = "";
		Client c = null;
		int option = 0; // Holds user option choice
		int mode = 0; // Holds user mode choice
		String filename; // Holds name of file to be transferred

		Scanner input = new Scanner(System.in);

		System.out.println("Welcome to Team 5's TFTP system.");

		/* Prompt for directory to transfer files to/from */
		File directory = new File("");
		while (!directory.isDirectory()) {
			System.out.println("Input the name of the directory to be used");
			dir = input.nextLine();
			directory = FileTransfer.searchForDir(new File("."), dir);
			if (directory != null) {
				dir = directory.getPath();
			} else {
				directory = new File(dir);
			}

			if (!directory.isDirectory()) {
				System.out.println("Invalid directory");
			}
		}
		System.out.println("Selected directory: " + dir);

		try {
			c = new Client(InetAddress.getByName(Configuration.HOSTNAME),
			               Configuration.SERVER_PORT, dir);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}

		while (option != 4) {
			System.out.println("\nChoose an action:");
			System.out.println("1. Read file from server.");
			System.out.println("2. Write file to server.");
			System.out.println("3. Change mode.");
			System.out.println("4. Exit.");

			try {
				option = Integer.parseInt(input.nextLine());
			} catch (NumberFormatException | NullPointerException e) {
				System.out.println("\nInvalid action chosen. Input 1..4 to " +
				                   "choose the options.");
				continue;
			}

			int valid;
			switch (option) {
				case 1: /* Read file operation. */
					System.out.println("\nInput the name of the file to be " +
					                   "transferred.");
					filename = input.nextLine();

					valid = FileTransfer.validateReceive(dir, filename);

					if (valid == 0) {
						c.transferFile(filename, option);
					} else if (valid == 6) {
						System.out.println("Chosen File to Read already " +
						                   "exists in chosen directory");
						System.out.println("File Name: " + filename);
						System.out.println("Directory: " + dir);
					} else if (valid == 2) {
						System.out.println("Access Violation for chosen " +
						                   "directory");
						System.out.println("Directory: " + dir);
					} else if (valid == 3) {
						System.out.println("Disk full or allocation exceeded " +
						                   "for chosen directory");
						System.out.println("Directory: " + dir);
					}
					continue;

				case 2: /* Write file operation. */
					System.out.println("\nInput the name of the file to be " +
					                   "transferred.");
					filename = input.nextLine();

					valid = FileTransfer.validateSend(dir, filename);

					if (valid == 0) {
						c.transferFile(filename, option);
					} else if (valid == 1) {
						System.out.println("Chosen File to Send does not " +
						                   "exist in chosen directory");
						System.out.println("File Name: " + filename);
						System.out.println("Directory: " + dir);
					} else if (valid == 2) {
						System.out.println("Access Violation for chosen " +
						                   "directory");
						System.out.println("Directory: " + dir);
					}
					continue;

				case 3: /* Change mode. */
					String modeStr;
					if (c.getMode() == 1) {
						modeStr = "normal quiet";
					} else if (c.getMode() == 2) {
						modeStr = "normal verbose";
					} else if (c.getMode() == 3) {
						modeStr = "test quiet";
					} else {
						modeStr = "test verbose";
					}

					System.out.println("\nCurrent mode: " + modeStr);

					System.out.println("\nChoose new mode:");
					System.out.println("1. normal quiet");
					System.out.println("2. normal verbose");
					System.out.println("3. test quiet");
					System.out.println("4. test verbose");

					do {

						/* User inputted a non-integer. */
						try {
							mode = Integer.parseInt(input.nextLine());
						} catch (NumberFormatException | NullPointerException e) {
							System.out.println("\nInvalid input. Input 1..4 " +
							                   "to choose a mode.");
							mode = 0;
							continue;
						}


						/* User inputted invalid integer. */
						if (mode > 4 || mode <= 0) {
							System.out.println("\nInvalid mode chosen. Input " +
							                   "1..4 to choose a mode.");
							continue;
						}

					} while (mode > 4 || mode <= 0);

					c.setMode(mode);
					continue;

				case 4: /* User wants to exit program. */
					continue;

				default:
					System.out.println("\nInvalid action chosen. Input 1..4 " +
					                   "to choose the options.");
					continue;
			}
		}
	}
}
