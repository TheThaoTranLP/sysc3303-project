/**
 * RRQ or WRQ packets are sent to request a read or write.
 *
 * Structure:
 * 2 bytes   string     1 byte   string   1 byte
 * ---------------------------------------------
 *| Opcode | Filename | 0      | Mode   | 0     |
 * ---------------------------------------------
 */

import java.net.*;
import java.util.Arrays;

public class RQPacket extends TFTPPacket {
	private int opcode;
	private String filename;
	private String mode;

	/**
	 * TFTPPacket constructor for RRQ/WRQ packets based on the components
	 *
	 * @param opcode   the code that represents the operation of the packet
	 * @param filename name of the file to be transferred
	 * @param mode     mode of the data to be transferred
	 * @param address  destination address of the packet
	 * @param port     destination port of the packet
	 */
	public RQPacket(int opcode, String filename, String mode,
	                InetAddress address, int port) {
		this.opcode = opcode;
		this.filename = filename;
		this.mode = mode;

		updatePacketData();

		this.udpPacket = new DatagramPacket(this.data, this.length, address,
		                                    port);
	}

	/**
	 * TFTPPacket constructor for RRQ/WRQ packets based on a datagram packet
	 *
	 * @param  udpPacket                 packet that contains a TFTP-compliant
	 *                                   byte array
	 * @throws IllegalArgumentException  if format of packet data is invalid
	 */
	public RQPacket(DatagramPacket udpPacket) throws IllegalArgumentException{
		byte[] data = Arrays.copyOf(udpPacket.getData(), udpPacket.getLength());

		if (data.length < 4 || data.length > 516) {
			throw new IllegalArgumentException("Invalid length expected " +
			                                   "between " + 4 + " and " + 516);
		}

		int nullTerminators = 0;

		for (byte b : data) {
			if (b == 0) {
				nullTerminators++;
			}
		}

		if (nullTerminators != 3) {
			throw new IllegalArgumentException("Illegal format");
		}

		this.udpPacket = udpPacket;
		this.data = udpPacket.getData();
		this.length = udpPacket.getLength();

		int dataInd = 1;

		this.opcode = this.data[dataInd++];

		int filenameStart = dataInd;
		for (; this.data[dataInd] != 0; dataInd++);
		this.filename = new String(this.data, filenameStart,
		                           dataInd - filenameStart);

		if (this.filename.length() == 0) {
			throw new IllegalArgumentException("Illegal filename");
		}

		dataInd++;

		int modeStart = dataInd;
		for (; this.data[dataInd] != 0; dataInd++);
		this.mode = new String(this.data, modeStart, dataInd - modeStart);

		if (!this.mode.equals("netascii") && !this.mode.equals("octal") &&
		    !this.mode.equals("mail")) {
			throw new IllegalArgumentException("Illegal mode " + this.mode);
		}
	}

	/**
	 * Updates the data and length fields.
	 */
	public void updatePacketData() {
		int dataInd = 0;
		this.data = new byte[4 + filename.length() + mode.length()];
		this.data[dataInd++] = 0;
		this.data[dataInd++] = (byte) opcode;

		for (byte b : filename.getBytes()) {
			this.data[dataInd++] = b;
		}

		this.data[dataInd++] = 0;

		for (byte b : mode.getBytes()) {
			this.data[dataInd++] = b;
		}

		this.data[dataInd] = 0;
		this.length = this.data.length;
	}

	public String getDetails() {
		String details = "";
		details += this + "\n";
		details += "Opcode: " + this.getOpcode() + "\n";
		details += "Filename: " + filename + "\n";
		details += "Mode: " + mode;

		return details;
	}

	public int getOpcode() {
		return opcode;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
		updatePacketData();
		udpPacket.setData(data);
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
		updatePacketData();
		udpPacket.setData(data);
	}

	public String toString() {
		return opcode == Configuration.RRQ ? "RRQ" : "WRQ";
	}
}
