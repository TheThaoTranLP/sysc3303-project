/**
 * Transfers files to and from socket.
 */

import java.io.*;
import java.net.*;
import java.util.*;
import java.nio.file.Files;

public class FileTransfer {
	private DatagramSocket sendReceiveSocket;
	private DatagramPacket sendPacket, receivePacket;

	private PacketHandler packetHandler;

	private String directory;
	private String filename;
	private InetAddress address;
	private int port;
	private boolean verbose;

	/**
	 * Constructor method for the file transfer worker.
	 *
	 * @param packetHandler  socket to send and receive packets
	 * @param filename       name of the file being transferred
	 * @param directory      name of the parent directory of the files to be
	 *                       transferred
	 * @param address        IP address of the socket being transferred with
	 * @param port           port number of the other socket
	 */
	public FileTransfer(PacketHandler packetHandler, String filename,
	                    String directory, InetAddress address, int port,
	                    boolean verbose) {
		this.filename = filename;
		this.directory = directory;
		this.address = address;
		this.port = port;
		this.verbose = verbose;

		this.packetHandler = packetHandler;
		sendReceiveSocket = packetHandler.getSocket();
	}

	/**
	 * Constructor method for the file transfer worker that doesn't set the
	 * destination address or port. These will be set once the transfer begins.
	 *
	 * @param packetHandler  socket to send and receive packets
	 * @param filename       name of the file being transferred
	 * @param directory      name of the parent directory of the files to be
	 *                       transferred
	 */
	public FileTransfer(PacketHandler packetHandler, String filename,
	                    String directory, boolean verbose) {
		this(packetHandler, filename, directory, null, -1, verbose);
	}

	/**
	 * Validate that the file to be sent exists and can be read.
	 *
	 * @return 0 if valid, otherwise returns the relevant TFTP error code
	 */
	public static int validateSend(String directory, String filename) {
		/* Search for file to be transferred in root directory. */
		File file = new File(directory, filename);

		if (!file.exists()) {
			/* File not found */
			return 1;
		} else if (!Files.isReadable(file.toPath())) {
			/* Access violation */
			return 2;
		} else {
			/* Valid file */
			return 0;
		}
	}

	/**
	 * Validate that the file to be received doesn't already exist and can be
	 * written.
	 *
	 * @return 0 if valid, otherwise returns the relevant TFTP error code
	 */
	public static int validateReceive(String directory, String filename) {
		File file = new File(directory, filename);
		File dir = file.getParentFile();

		if (!Files.isWritable(dir.toPath())) {
			/* Access violation */
			return 2;
		} else if (file.exists()) {
			/* File already exists */
			return 6;
		} else {
			try {
				/* Check if there is any space on the disk */
				file.createNewFile();
				long space = file.getUsableSpace();
				file.delete();

				if (space == 0) {
					/* Disk full or allocation exceeded. */
					return 3;
				}
			} catch (IOException e) {
				return 3;
			}

			/* Valid file */
			return 0;
		}
	}

	/**
	 * Sending a file to the specified address and port.
	 */
	public void sendFile() {
		byte dataBlock[];
		int size = 0, bytesToRead = 0;
		int blockNum = 1;
		boolean sendEmpty = false;

		try {
			File file = new File(directory, filename);

			/* Open file */
			FileInputStream inputStream = new FileInputStream(file);

			boolean justListen = false;

			/* Read and send blocks of bytes until end of file is reached */
			do {
				if (!justListen) {
					bytesToRead = inputStream.available();
					size = bytesToRead < Configuration.BLOCK_SIZE ? bytesToRead:
					       Configuration.BLOCK_SIZE;

					if (bytesToRead > 0) {
						dataBlock = new byte[size];

						/* Read block of data and populate a packet with the data and
						 * it's corresponding block number*/
						inputStream.read(dataBlock);
					} else {
						dataBlock = new byte[0];
					}

					/* Send DATA packet to destination and wait for ACK packet. */
					packetHandler.send(new DataPacket(blockNum, dataBlock, address,
					                                  port));
				} else {
					justListen = false;
				}

				/* Receive times out after 5 seconds */
				TFTPPacket tftpPacket = packetHandler.receive(4, 5000);

				if (tftpPacket == null) {
					inputStream.close();
					System.out.println("File transfer not completed.");
					return;
				} else if (tftpPacket.toString().equals("ERROR")) {
					inputStream.close();
					ErrorPacket errorPacket = (ErrorPacket) tftpPacket;
					System.out.println(errorPacket.getErrMsg());
					System.out.println("File transfer not completed.");
					return;
				} else {
					DatagramPacket udpPacket = tftpPacket.getUdpPacket();

					/* If an ACK was received from an incorrect port or address,
					 * then send an ERROR packet back to that sender, and go
					 * back to listening for an ACK.  */
					if (!address.equals(udpPacket.getAddress()) ||
					    port != udpPacket.getPort()) {
						packetHandler.unknownTidError(address, port,
						                              udpPacket.getAddress(),
						                              udpPacket.getPort());
						justListen = true;
						continue;
					}

					if (tftpPacket.toString().equals("ACK")) {
						AckPacket ackPacket = (AckPacket) tftpPacket;
						/*If block number of received packet is not correct, send
						 * an ERROR packet. */
						if (ackPacket.getBlockNum() > blockNum) {
							packetHandler.incorrectBlockNum(Configuration.ACK,
							                                blockNum,
							                                ackPacket.getBlockNum(),
							                                address, port);
							System.out.println("File transfer not completed.");
							return;
						} else if (ackPacket.getBlockNum() < blockNum) {
							/* If duplicate ACK packet received, wait for
							 * another ACK. */
							justListen = true;
							continue;
						}
					}
				}
				blockNum++;
			} while (inputStream.available() > 0 || bytesToRead == 512);

			inputStream.close();
		} catch (IOException e) {
			System.out.println("Invalid file.");
			return;
		}

		System.out.println("File transfer completed successfully.");
	}

	/**
	 * Receive a transferred file from a client.
	 */
	public void receiveFile() {
		try {
			File file = new File(directory, filename);

			/* Open file */
			FileOutputStream outputStream = new FileOutputStream(file);
			int blockNum = 1;

			/* Receive and write data blocks to file until end of file is
			 * reached */
			while (true) {
				/* Waits to receive a DATA packet. */
				TFTPPacket tftpPacket = packetHandler.receive(3, 5000);

				if (tftpPacket == null) {
					outputStream.close();
					file.delete();
					System.out.println("File transfer not completed.");
					return;
				} else if (tftpPacket.toString().equals("ERROR")) {
					outputStream.close();
					file.delete();
					ErrorPacket errorPacket = (ErrorPacket) tftpPacket;
					System.out.println(errorPacket.getErrMsg());
					System.out.println("File transfer not completed.");
					return;
				} else if (tftpPacket.toString().equals("DATA")) {
					/* Received packet has data. */
					DataPacket dataPacket = (DataPacket) tftpPacket;
					DatagramPacket udpPacket = dataPacket.getUdpPacket();

					/* If no address and no port were specified, then the
					 * destination address and TID/port will be determined based
					 * on the the first data packet's source */
					if (address == null && port == -1) {
						address = udpPacket.getAddress();
						port = udpPacket.getPort();
					}

					/* If a DATA packet was received from an incorrect port or
					 * address, then send an ERROR packet back to that sender,
					 * and go back to listening for a DATA packet. */
					else if (!address.equals(udpPacket.getAddress()) ||
					           port != udpPacket.getPort()) {
						packetHandler.unknownTidError(address, port,
						                              udpPacket.getAddress(),
						                              udpPacket.getPort());
						continue;
					}

					/*If block number of received packet is not correct, send
					 * an ERROR packet. */
					if (dataPacket.getBlockNum() > blockNum) {
						packetHandler.incorrectBlockNum(Configuration.DATA,
						                                blockNum,
						                                dataPacket.getBlockNum(),
						                                address, port);
						continue;
					} else if (dataPacket.getBlockNum() == blockNum - 1) {
						blockNum = dataPacket.getBlockNum();
					} else if (dataPacket.getBlockNum() < blockNum) {
						continue;
					}

					/* If received packet contains data. */
					if (dataPacket.getLength() > 4) {
						byte[] dataBlock = dataPacket.getDataBlock();
						if (dataBlock.length <= file.getUsableSpace()) {
							/* Write data received to the file. */
							outputStream.write(dataPacket.getDataBlock());
						} else {
							/* Not enough free space. Abort the file transfer,
							 * send an error packet, and delete the half-written
							 * file. */
							outputStream.close();
							file.delete();
							packetHandler.diskFullError(address, port);
							return;
						}
					}

					/* Send ACK packet to source. */
					packetHandler.send(new AckPacket(dataPacket.getBlockNum(),
					                                 address, port));
				}

				/* If the data packet was less than the maximum length, end the
				 * file transfer */
				if (tftpPacket.getLength() < Configuration.MAX_PACKET_SIZE) {
					break;
				}

				blockNum++;
			}

			outputStream.close();
		} catch (IOException e) {
			System.out.println("Invalid file.");
			return;
		}

		System.out.println("File transfer completed successfully.");
	}

	/**
	 * Recursively searches the root directory for the file to be transferred.
	 * It also checks if the file is in the right folder (Server folder if
	 * reading and Client folder if writing).
	 *
	 * @param  file      root file to begin search
	 * @param  dir       name of parent directory of the file to be transferred
	 * @param  filename  name of the file to be transferred
	 * @return           file corresponding to the directory and filename
	 */
	public static File searchForFile(File file, String dir, String filename) {
		File returnFile = null;

		if (file.isDirectory()) {
			/* Iterates through files in directory. */
			for (File f : file.listFiles()) {
				returnFile = searchForFile(f, dir, filename);

				if (returnFile != null) { /* File was found. */
					return returnFile;
				}
			}
		}
		else {
			if (file.getName().equals(filename) &&
				file.getParentFile().getName().equals(dir)) {
				return file;
			}
		}

		return returnFile;
	}

	/**
	 * Recursively searches root directory for the directory that the file will
	 * be transferred to.
	 *
	 * @param  file  root file to begin search
	 * @param  dir   name of directory
	 * @return       directory that corresponds with the given directory name
	 */
	public static File searchForDir(File file, String dir) {
		File returnFile = null;

		if (file.isDirectory()) {
			if (file.getName().equals(dir)) {
				return file;
			}

			/* Iterates through files in directory. */
			for (File f : file.listFiles()) {
				returnFile = searchForDir(f, dir);
				if (returnFile != null) { /* Directory was found. */
					return returnFile;
				}
			}
		}

		return returnFile;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
}
