/**
 * Data packet for TFTP systems.
 *
 * Structure:
 * 2 bytes   2 bytes   n bytes
 * ----------------------------
 *| Opcode | Block # | Data    |
 * ----------------------------
 */

import java.util.Arrays;
import java.net.*;

public class DataPacket extends TFTPPacket {
	int blockNum;
	byte[] dataBlock;

	/**
	 * TFTPPacket constructor for DATA packets based on the components.
	 *
	 * @param blockNum   an integer representing the block number of the data
	 * @param dataBlock  a byte array from 0-512 bytes long that represents the
	 *                   data being transferred
	 * @param address    destination address of the packet
	 * @param port       destination port of the packet
	 *
	 */
	public DataPacket(int blockNum, byte[] dataBlock, InetAddress address,
	                  int port) {
		this.blockNum = blockNum;
		this.dataBlock = dataBlock;
		updatePacketData();
		this.udpPacket = new DatagramPacket(this.data, this.length, address,
		                                    port);
	}

	/**
	 * TFTPPacket constructor for DATA packets based on a datagram packet
	 *
	 * @param  packetData                packet that contains a TFTP-compliant
	 *                                   byte array
	 * @throws IllegalArgumentException  if format of packet data is invalid
	 */
	public DataPacket(DatagramPacket udpPacket) throws IllegalArgumentException {
		byte[] data = Arrays.copyOf(udpPacket.getData(), udpPacket.getLength());

		if (data.length < 4 || data.length > 516) {
			throw new IllegalArgumentException("Invalid length expected " +
			                                   "between " + 4 + " and " + 516);
		}

		int nullTerminators = 0;

		for (byte b : Arrays.copyOf(data, 4)) {
			if (b == 0) {
				nullTerminators++;
			}
		}

		if (nullTerminators > 2) {
			throw new IllegalArgumentException("Invalid format");
		}

		this.udpPacket = udpPacket;
		this.data = Arrays.copyOf(data, data.length);
		this.length = udpPacket.getLength();

		this.blockNum = ((data[2] & 0xFF) << 8) | (data[3] & 0xFF);
		this.dataBlock = Arrays.copyOfRange(this.data, 4, this.length);
	}

	/**
	 * Updates the data and length fields.
	 */
	public void updatePacketData() {
		int dataInd = 0;
		this.data = new byte[4 + dataBlock.length];
		this.data[dataInd++] = 0;
		this.data[dataInd++] = 3;
		this.data[dataInd++] = (byte) (blockNum >> 8);
		this.data[dataInd++] = (byte) blockNum;

		for (byte b : dataBlock) {
			this.data[dataInd++] = b;
		}

		this.length = this.data.length;
	}

	public String getDetails() {
		String details = "";
		details += this + "\n";
		details += "Opcode: " + this.getOpcode() + "\n";
		details += "BlockNum: " + blockNum + "\n";
		details += "DataLength: " + dataBlock.length;

		return details;
	}

	public int getBlockNum() {
		return blockNum;
	}

	public void setBlockNum(int blockNum) {
		this.blockNum = blockNum;
		updatePacketData();
		udpPacket.setData(data);
	}

	public byte[] getDataBlock() {
		return dataBlock;
	}

	public void setDataBlock(byte dataBlock[]) {
		this.dataBlock = Arrays.copyOf(dataBlock, dataBlock.length);
		updatePacketData();
		udpPacket.setData(data);
	}

	public String toString() {
		return "DATA";
	}
}
