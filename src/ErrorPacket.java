/**
 * ERROR packets are sent to indicate an error within a received packet.
 *
 * Structure:
 * 2 bytes   2 bytes     string   1 byte
 * -------------------------------------
 *| Opcode | ErrorCode | ErrMsg | 0     |
 * -------------------------------------
 */

import java.net.*;
import java.util.Arrays;

public class ErrorPacket extends TFTPPacket {
	private int errCode;
	private String errMsg;

	/**
	 * TFTPPacket constructor for ERROR packets based on the components
	 *
	 * @param errCode  the code that represents the error
	 * @param errMsg   the error message
	 * @param address  destination address of the packet
	 * @param port     destination port of the packet
	 */
	public ErrorPacket(int errCode, String errMsg, InetAddress address,
	                   int port) {
		int dataInd = 0;
		byte[] data = new byte[5 + errMsg.length()];
		data[dataInd++] = 0;
		data[dataInd++] = 5;
		data[dataInd++] = (byte) (errCode >> 8);
		data[dataInd++] = (byte) errCode;

		for (byte b : errMsg.getBytes()) {
			data[dataInd++] = b;
		}

		data[dataInd] = 0;

		this.errCode = errCode;
		this.errMsg = errMsg;

		this.data = data;
		this.length = data.length;
		this.udpPacket = new DatagramPacket(this.data, this.length, address,
		                                    port);
	}

	/**
	 * TFTPPacket constructor for ERROR packets based on a datagram packet
	 *
	 * @param  udpPacket                 packet that contains a TFTP-compliant
	 *                                   byte array
	 * @throws IllegalArgumentException  if format of packet data is invalid
	 */
	public ErrorPacket(DatagramPacket udpPacket) throws IllegalArgumentException{
		byte[] data = Arrays.copyOf(udpPacket.getData(), udpPacket.getLength());

		if (data.length < 5 || data.length > 516) {
			throw new IllegalArgumentException("Invalid length expected " +
			                                   "between " + 5 + " and " + 516);
		}

		int nullTerminators = 0;

		for (byte b : data) {
			if (b == 0) {
				nullTerminators++;
			}
		}

		if (nullTerminators < 3 || nullTerminators > 4) {
			throw new IllegalArgumentException("Illegal format");
		}

		this.udpPacket = udpPacket;
		this.data = udpPacket.getData();
		this.length = udpPacket.getLength();

		this.errCode = (((int) this.data[2]) << 8) + this.data[3];
		this.errMsg = new String(this.data, 4, this.length - 5);
	}

	public String getDetails() {
		String details = "";
		details += this + "\n";
		details += "Opcode: " + this.getOpcode() + "\n";
		details += "ErrCode: " + errCode + "\n";
		details += "ErrMsg: " + errMsg;

		return details;
	}

	public int getErrCode() {
		return errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public String toString() {
		return "ERROR";
	}
}
