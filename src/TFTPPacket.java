/**
 * Packet for TFTP systems. TFTP supports multiple types of packets which
 * constitute different headers.
 */

import java.net.*;

public abstract class TFTPPacket {
	protected byte data[];
	protected int length;
	protected DatagramPacket udpPacket;

	public byte[] getData() {
		return data;
	}

	public int getLength() {
		return length;
	}

	public int getOpcode() {
		return ((data[0] & 0xFF) << 8) | (data[1] & 0xFF);
	}

	public DatagramPacket getUdpPacket() {
		return udpPacket;
	}

	public abstract String toString();

	/**
	 * Build a string that contains the TFTP details of the packet.
	 *
	 * @return a string with details in key-value pairs
	 */
	public abstract String getDetails();
}
