import java.util.Scanner;

/**
 * This class represents the thread that handles checking for the user entering
 * the quit comand. The command to quit is just the letter "q" and should be
 * entered on STDIN.
 */

public class ShutdownHandler implements Runnable {
	Server server;

	/**
	 * Constructor for the shutdown handler.
	 *
	 * @param server the server thread that this application will notify once
	 *               the quit command is entered
	 */
	public ShutdownHandler(Server server) {
		this.server = server;
	}

	/**
	 * Wait for the user to enter the quit command and then notify the server.
	 */
	public void run() {
		Scanner input = new Scanner(System.in);
		String userInput = "";

		/* Wait until the command "q" is entered on STDIN */
		while (!userInput.toLowerCase().equals("q") &&
		       !userInput.toLowerCase().equals("quit") &&
		       !userInput.toLowerCase().equals("shutdown"))
			userInput = input.next();

		/* Notify the server to quit */
		server.setQuit();
	}
}
