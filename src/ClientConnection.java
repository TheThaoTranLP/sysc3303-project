/**
 * This class represents the connection that the server creates between itself
 * and a client to facilitate file transfer.
 */

import java.net.*;

public class ClientConnection extends Thread {
	private int rqOpcode;
	private InetAddress clientAddress;
	private int clientPort;

	private PacketHandler packetHandler;
	private FileTransfer transfer;

	/**
	 * Constructor for the client connection. Requires the client's address and
	 * port in order to establish the connection.
	 *
	 * @param rqOpcode      identify the transfer as a read (1) or write (2)
	 * @param filename      name of file to be transferred
	 * @param directory     location of file to be transferred
	 * @param clientAddress address of destination/client
	 * @param clientPort    port of destination/client
	 * @param verbose       boolean specifying whether to print packet
	 *                      information during transfer
	 */
	public ClientConnection(int rqOpcode, String filename, String directory,
	                        InetAddress clientAddress, int clientPort,
	                        boolean verbose) {
		this.rqOpcode = rqOpcode;
		this.clientAddress = clientAddress;
		this.clientPort = clientPort;

		packetHandler = new PacketHandler(verbose);
		transfer = new FileTransfer(packetHandler, filename, directory,
		                            clientAddress, clientPort, verbose);
	}

	/**
	 * Transfer the specified file from server to client (READ) or from client
	 * to server (WRITE).
	 */
	public void run() {
		/* READ: File transfer from server to client */
		if (rqOpcode == 1) {
			transfer.sendFile();
		}

		/* WRITE: File transfer from client to server */
		else if (rqOpcode == 2) {
			packetHandler.send(new AckPacket(0, clientAddress, clientPort));
			transfer.receiveFile();
		}
	}

	public InetAddress getClientAddress() {
		return clientAddress;
	}

	public int getClientPort() {
		return clientPort;
	}
}
