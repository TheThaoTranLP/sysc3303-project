/**
 * This class is the server side for a file transfer system based on TFTP. The
 * server has a multithreaded architecture and is capable of supporting multiple
 * concurrent read and write connections with different clients. Once started,
 * the server will run until it receives a shutdown ("q") command from the
 * server operator.
 */

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.io.*;
import java.net.*;

public class Server {
	private PacketHandler requestHandler;

	private String directory;
	private boolean verbose;

	private boolean quit;
	private HashSet<ClientConnection> connectionThreads;

	/**
	 * Constructor method for the Server class.
	 *
	 * @param port      The port that the server will listen on.
	 * @param directory The directory for storing server-side files.
	 * @param verbose   Boolean specifying whether to print packet information
	 *                  during transfer
	 */
	public Server(int port, String directory, boolean verbose) {
		this.directory = directory;
		this.verbose = verbose;
		this.quit = false;

		requestHandler = new PacketHandler(port, verbose);
		connectionThreads = new HashSet<ClientConnection>();
	}

	public synchronized void setQuit() {
		quit = true;
	}

	public synchronized boolean getQuit() {
		return quit;
	}

	/**
	 * Wait for a request packet, then delegate the file transfer to a worker.
	 */
	public void work() {
		RQPacket rqPacket = null;
		boolean firstWait = true;

		/* Continually wait for a request packet, pausing every second to check
		 * if the quit command has been entered */
		while(rqPacket == null) {
			/* Parse the received request packet data */
			rqPacket = (RQPacket) requestHandler.receive(Configuration.RQ,
			                                             1000, firstWait);
			if (firstWait) {
				firstWait = false;
			}

			/* If the receive timed out and the quit variable has been set then
			 * stop waiting for requests and wait for all remaining client
			 * connection threads to complete execution */
			if (rqPacket == null && getQuit()) {
				Iterator<ClientConnection> threadIter = connectionThreads.iterator();
				while (threadIter.hasNext()) {
					try {
						/* Individually wait for each client connecion thread to
						 * finish execution */
						threadIter.next().join();
						threadIter.remove();
					} catch (InterruptedException e) {
						e.printStackTrace();
						System.exit(1);
					}
				}
				return;
			}
		}
		DatagramPacket udpPacket = rqPacket.getUdpPacket();
		InetAddress rqAddress = udpPacket.getAddress();
		int rqPort = udpPacket.getPort();

		int opcode = rqPacket.getOpcode();
		String filename = rqPacket.getFilename();

		/* Stop tracking any threads that have completed execution */
		Iterator<ClientConnection> threadIter = connectionThreads.iterator();
		while (threadIter.hasNext()) {
			if (!threadIter.next().isAlive()) threadIter.remove();
		}

		threadIter = connectionThreads.iterator();
		while (threadIter.hasNext()) {
			ClientConnection c = threadIter.next();
			InetAddress connectionAddress = c.getClientAddress();
			int connectionPort = c.getClientPort();
			if (rqAddress.equals(connectionAddress) && rqPort == connectionPort)
				return;
		}

		int res = 0;
		switch (opcode) {
			case Configuration.RRQ:
				res = FileTransfer.validateSend(directory, filename);
				break;
			case Configuration.WRQ:
				res = FileTransfer.validateReceive(directory, filename);
				break;
		}

		/* TODO: Send error correct error packet */
		if (res != 0) {
			switch (res) {
				case 1:
					requestHandler.fileNotFoundError(rqAddress, rqPort);
					break;
				case 2:
					requestHandler.noAccessError(rqAddress, rqPort);
					break;
				case 3:
					requestHandler.diskFullError(rqAddress, rqPort);
					break;
				case 6:
					requestHandler.fileExistsError(rqAddress, rqPort);
					break;
			}
			return;
		}

		/* Delegate file transfer to a worker */
		ClientConnection connection;
		connection = new ClientConnection(opcode, filename, directory,
		                                  rqAddress, rqPort, verbose);
		connection.start();
		connectionThreads.add(connection);
	}

	public static void main(String args[]) {
		boolean verbose = false;
		String dir = "";

		Scanner input = new Scanner(System.in);

		/* Prompt for verbose or quiet mode */
		while(true) {
			System.out.println("Choose a mode to run Server:");
			System.out.println("1. Verbose.");
			System.out.println("2. Quiet.");

			switch (input.nextLine()) {
				case "1":
					verbose = true;
					System.out.println("Verbose mode selected.");
					break;
				case "2":
					verbose = false;
					System.out.println("Quiet mode selected.");
					break;
				default:
					System.out.println("Invalid input.");
					continue;
			}

			break;
		}

		/* Prompt for directory to transfer files to/from */
		File directory = new File("");
		while (!directory.isDirectory()) {
			System.out.println("\nInput the name of the directory to be used");
			dir = input.nextLine();
			directory = FileTransfer.searchForDir(new File("."), dir);
			if (directory != null) {
				dir = directory.getPath();
			} else {
				directory = new File(dir);
			}

			if (!directory.isDirectory()) {
				System.out.println("Invalid directory");
			}
		}
		System.out.println("Selected directory: " + dir + "\n");

		Server server = new Server(Configuration.SERVER_PORT, dir, verbose);

		/* Create the shutdown/quit handling thread */
		ShutdownHandler shutdownHandler = new ShutdownHandler(server);
		Thread t = new Thread(shutdownHandler);
		t.start();

		/* Wait for and service requests until the server has been shut down */
		while (!server.getQuit())
			server.work();
	}
}
