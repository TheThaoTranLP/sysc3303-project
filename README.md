# SYSC3303 Project

This is the repository for the SYSC3303A Concurrent Systems Course Project.

### Project Members
*	**Thao-Tran Le-Phuong | 100997443**

*	Keegan Wills | 100966515

*	Paul Seguin | 100887782

*	Igor Veselinovic | 101011081

*	Ethan Morrill | 100956097

---

##Run the program

###Eclipse

1. Run Server class.
2. Run ErrorSimulator class.
3. Run Client class.

###Console

1. Change the directory into the src directory.
2. Run `javac *.java` to compile the files.
3. Run `java Server` to start the server.
3. Run `java ErrorSimulator` in a new terminal to start the error simulator.
4. Run `java Client` in a new terminal to start the client.


---

**The three act the same whether on console or eclipse**

---

####Server

The Server console will give options between Verbose and Quiet.

Followed by the oppurtunity to select a working directory for the server.

All reads and writes will be performed in the selected directory

Passing q at anytime will quit out of the server

---

####Client

The Client will first request a chosen working directory.
After a valid working directory is chosen, the user will be prompted to choose
from the following menu:
1. Read file from server.
   * A file from the server will be transferred into the directory chosen by the
     user.
2. Write file to server.
   * A file from the directory chosen by the user will be transferred to the
     server.
3. Change mode.
   * The user will be prompter to choose a mode from the following menu:
     1. normal quiet
     2. normal verbose
     3. test quiet
     4. test verbose
4. Exit.
   * Closes the client.

---
	
####ErrorSimulator

The Error Simulator acts as a proxy between the clients and the server and can
simulate various errors in packets. When it starts, the user will be prompted to
select Quiet or Verbose Mode. Then it will prompt the user for an error to
simulate.

First it will give an option between:
1. No error
2. Network error
   * If a network error is chosen, the user will be prompted to choose from the
     following menu:
     1. Duplicate packet (RRQ/WRQ)
     2. Duplicate packet (DATA)
     3. Duplicate packet (ACK)
     4. Delay packet (RRQ/WRQ)
     5. Delay packet (DATA)
     6. Delay packet (ACK)
     7. Drop packet (RRQ/WRQ)
     8. Drop packet (DATA)
     9. Drop packet (ACK)
3. TFTP error
   * If a TFTP error is chosen, the user will be prompted to choose from the
     following menu:
     1. Invalid opcode (RRQ/WRQ)
     2. Invalid opcode (DATA)
     3. Invalid opcode (ACK)
     4. Invalid RRQ/WRQ mode
     5. Invalid block number (DATA)
     6. Invalid block number (ACK)
     7. Invalid format (RRQ/WRQ)
     8. Invalid format (DATA)
     9. Invalid format (ACK)
     10. Unknown transfer ID (DATA)
     11. Unknown transfer ID (ACK)


---
	
#Project State


### Iteration 1

* *README by Keegan

* *UML by Ethan & Paul

* *Code base by Thao & Igor

### Iteration 2

* Server Multi Threading by Igor

* Server Error Handling by Keegan

* Client Error Handling by Thao

* Error Simulator by Paul and Ethan

* Diagrams and Documentation by Keegan, Igor and Paul

### Iteration 3

* New Error packet Creation and handling by Keegan

* Client Side Handling by Paul and Ethan

* Server Side Handling by Igor and Thao

* Diagrams and Documentation by Paul and Ethan

### Iteration 4

* README by Keegan & Paul

* Error Simulator update by Ethan

* Network error handling by Thao & Igor

* Diagrams by Paul
